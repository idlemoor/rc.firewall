# rc.firewall

A simple firewall script using iptables

  * ipv4 and ipv6
  * independent of forwarding (see /etc/rc.d/rc.ip_forward)
  * no NAT

### Installation

  * Put rc.firewall in /etc/rc.d and ensure it is executable
  * Edit the configuration file rc.firewall.conf and put it in /etc/rc.d

You don't need to do anything else (in particular, you don't need to edit
rc.local) because rc.inet2 is already setup to call rc.firewall if it exists.
